#!/usr/bin/env python3

# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from flask import Flask
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint

from . import error, route
from .library import KodeKemendagriConverter, MapJDS


def create_app(debug=False):
    app = Flask(__name__)

    # Python Flask Cors Issue
    # https://stackoverflow.com/questions/28461001/python-flask-cors-issue
    CORS(app, support_credentials=True)

    app.config.from_object('api.config.Config')
    app.url_map.converters['kode_kemendagri'] = KodeKemendagriConverter
    app.debug = debug
    app.config['map_jds'] = MapJDS(
        *[
            app.config[key]
            for key in [
                'DB_NAME',
                'DB_USERNAME_READONLY',
                'DB_PASSWORD_READONLY',
                'DB_SCHEMA',
                'DB_HOST',
                'DB_PORT',
                'SSH_HOST',
                'SSH_PORT',
                'SSH_USERNAME',
                'SSH_PASSWORD',
                'SSH_PKEY_FILEPATH'
            ]
        ],
        debug
    )
    app.register_blueprint(
        get_swaggerui_blueprint(
            '/doc',
            # Pergunakan http://editor.swagger.io/ untuk validasi json
            '/static/swagger.json',
            blueprint_name='doc'
        ),
        url_prefix='/doc/'
    )
    app.register_blueprint(error.app)
    app.register_blueprint(route.app)
    return app
