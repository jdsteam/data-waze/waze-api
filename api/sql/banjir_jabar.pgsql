-- MIT License

-- Copyright (c) 2019 Pipin Fitriadi

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

-- explain analyze
select a.report_rating "reportRating"
	, a.reliability
	, a."type"
	, a.country
	, a.subtype
	, a.city
	, maps.json_location "location"
	, a.pub_millis "pubMillis"
	, a.uuid
	, a.magvar
	, a.road_type "roadType"
	, a.confidence
	, a.n_thumbs_up "nThumbsUp"
	, a.street
	, df.date_created created_time
	, df.start_time_locale start_time
	, df.end_time_locale end_time
	, df.kode_kabupaten
	, wmkbk.kemendagri_kabupaten_nama nama_kabupaten
from waze.waze.data_files df
join waze.waze.wilayah_merge_kemendagri_2017_bps_2018_kabupaten wmkbk
	on df.kode_kabupaten = wmkbk.kemendagri_kabupaten_kode
join lateral (
		select a.*
		from waze.waze.alerts a
		where a.subtype in (
			'HAZARD_WEATHER_HAIL'
			, 'HAZARD_WEATHER_HEAVY_RAIN'
			, 'HAZARD_WEATHER_FLOOD'
			, 'HAZARD_WEATHER_MONSOON'
			, 'HAZARD_WEATHER_FREEZING_RAIN'
		)
	) a
	on df.id = a.datafile_id
join lateral (
		select ST_MakePoint(
				("location"->>'x')::numeric
				, ("location"->>'y')::numeric
			)::geography gis_location
			, coalesce(
				jsonb_build_array(
					"location"->'x'
					, "location"->'y'
				)
				, '{}'::jsonb
			) json_location
	) maps
	on true
where df.start_time_locale >= :start_time
	and df.start_time_locale <= :end_time
;
