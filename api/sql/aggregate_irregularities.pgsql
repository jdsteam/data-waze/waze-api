-- MIT License

-- Copyright (c) 2019 Pipin Fitriadi

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

--select *
--from waze.aggregate_irregularities
--;

--delete from waze.aggregate_irregularities;

--vacuum waze.aggregate_irregularities;

with var as (
	select (
			:start_time
			-- timestamp '2019-06-11'
		)::timestamp "date"
		, (
			:kemendagri_kabupaten_kode
			-- '32.01'
		) kemendagri_kabupaten_kode
)
, will_be_inserted as (
	select aggregate_table.*
	from var
		, lateral (
			select generate_series(
					var."date"
					, var."date" + '23 hour'
					, interval '1 hour'
				) "time"
		) t
		, lateral (
			with selected_data_files as (
				select id
					, start_time_locale
					, kode_kabupaten
					, t."time"
				from waze.data_files
				where start_time_locale
						between t."time"
							and t."time" + '1 hour'
					and kode_kabupaten = var.kemendagri_kabupaten_kode
			)
			, selected_irregularities as (
				select i.*
					, k.kemendagri_kabupaten_kode
					, k.kemendagri_kabupaten_nama
				from selected_data_files d
				join waze.irregularities i
					on d.id = i.datafile_id
				join waze.wilayah_merge_kemendagri_2017_bps_2018_kabupaten k
					on d.kode_kabupaten = k.kemendagri_kabupaten_kode
			)
			select date("time")  
				, "time"
				, kemendagri_kabupaten_kode
				, kemendagri_kabupaten_nama
				, street
				, jam_level
				, cause_type
				, ST_AsGeoJSON(
					ST_LineMerge(
						ST_Collect(l."line")
					)
				)::jsonb geometry
				, avg("length") avg_length
				, avg(delay_seconds) avg_delay_seconds
				, avg(seconds) avg_seconds
				, avg(speed) avg_speed
				, avg(regular_speed) avg_regular_speed
				, count(s.id) total_records
			from selected_irregularities s
			join lateral (
				with point as (
					select id
						, ST_SetSRID(
							ST_MakePoint(x, y), 4326
						) geometry
					from jsonb_to_recordset("line"->'line')
						as specs(x float8, y float8)
				)
				select id
					, ST_MakeLine(geometry) "line"
				from point
				group by id
			) l
				on s.id = l.id
			group by kemendagri_kabupaten_kode
				, kemendagri_kabupaten_nama
				, street
				, jam_level
				, cause_type
		) aggregate_table
	order by aggregate_table."time"
		, aggregate_table.kemendagri_kabupaten_nama
		, aggregate_table.street
		, aggregate_table.jam_level
		, aggregate_table.cause_type
	-- limit 1
)
, insert_processed as (
	insert into waze.aggregate_irregularities (
		"date" 
		, "time"
		, kemendagri_kabupaten_kode
		, kemendagri_kabupaten_nama
		, street
		, jam_level
		, cause_type
		, geometry
		, avg_length
		, avg_delay_seconds
		, avg_seconds
		, avg_speed
		, avg_regular_speed
		, total_records
	)
	select *
	from will_be_inserted
	where not exists(
		select *
		from waze.aggregate_irregularities w
			, var
		where w."date" = var."date"
			and w.kemendagri_kabupaten_kode = var.kemendagri_kabupaten_kode
		order by w."date"
			, w.kemendagri_kabupaten_kode
		limit 1
	)
	returning *
)
select count(*) rows_affected
from insert_processed
;
