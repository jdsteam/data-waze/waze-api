-- MIT License

-- Copyright (c) 2019 Pipin Fitriadi

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

with var as (
	select cast(
			:start_time
            -- '2019-10-11 12:00:00'::timestamp
			-- null
			as timestamp
		) start_time
		, cast(
			:end_time
			-- '2019-10-11 17:01:00'::timestamp
			-- null
			as timestamp
		) end_time
        , (
			:kode_kabupaten
			-- '["32.01", "32.02"]'
			-- null
        )::jsonb kode_kabupaten
        , (
			:page
			-- 1
        ) page
        , (
			:array_size
			-- 800
        ) array_size
        , ST_MakePoint(
			cast(
				:longitude
                -- 107.608154
				-- null
				as numeric
			)
			, cast(
				:latitude
 				-- -6.921294
				-- null
				as numeric
			)
		)::geography point
		, (
			(
				:radius_km
				-- 1.
			) * 1000
		) radius_m
		, (
			:map_area
			-- '{
			-- 	"type": "Polygon",
			-- 	"coordinates": [
			-- 	[
			-- 		[
			-- 			90.3515625,
			-- 			-3.162455530237848
			-- 		],
			-- 		[
			-- 			114.2578125,
			-- 			-25.16517336866393
			-- 		],
			-- 		[
			-- 			107.608154,
			-- 			-6.921294
			-- 		],
			-- 		[
			-- 			107.9296875,
			-- 			17.97873309555617
			-- 		],
			-- 		[
			-- 			90.3515625,
			-- 			-3.162455530237848
			-- 		]
			-- 		]
			-- 	]
			-- }'
			-- null
		) map_area
)
, response_merged as (
	select s.*
		, w.*
		, d.*
	from waze.alerts w
		, waze.data_files d
		, var
		, lateral (
			select ST_MakePoint(
					(w."location"->>'x')::numeric
					, (w."location"->>'y')::numeric
				)::geography "area"
		) m
		, lateral (
			select (
					$checking
					--
--					true
					--
--					jsonb_build_array(d.kode_kabupaten) <@ var.kode_kabupaten
					--
--					ST_DWithin(
--						m."area"
--						, var.point
--						, var.radius_m
--					)
					--
--					ST_Intersects(
--						m."area"
--						, st_geomfromgeojson(var.map_area)
--					)
				) checking
		) c
		, lateral (
			select (
					(
						checking is null
						and true
					)
					or (
						checking is not null
						and checking
					)
				) status
		) s
	where datafile_id = d.id
		-- Optimizing queries on a range of timestamps (two columns):
		-- https://dba.stackexchange.com/questions/39589/optimizing-queries-on-a-range-of-timestamps-two-columns
		and tsrange(
				d.start_time_locale
				, d.end_time_locale
				, '[]'
			)
			<@ tsrange(
				var.start_time
				, var.end_time
				, '[]'
			)
)
, response_data as (
	select report_rating "reportRating"
		, reliability
		, "type"
		, country
		, subtype
		, city
		, l."location"
		, pub_millis "pubMillis"
		, uuid
		, magvar
		, road_type "roadType"
		, confidence
		, n_thumbs_up "nThumbsUp"
		, street
		, date_created created_time
		, start_time_locale start_time
		, end_time_locale end_time
		, kode_kabupaten
		, nama_kabupaten
	from response_merged
		, lateral (
			select coalesce(
					jsonb_build_array(
						"location"->'x'
						, "location"->'y'
					)
					, '{}'::jsonb
				) "location"
		) l
	where status
	limit (
		select array_size
		from var
	)
	offset (
		select (page - 1) * array_size
		from var
	)
)
-- select *
select coalesce(
		jsonb_agg(response_data)
		, '[]'::jsonb
	)
from response_data
;
