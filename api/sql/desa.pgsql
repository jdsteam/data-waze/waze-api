-- MIT License

-- Copyright (c) 2019 Pipin Fitriadi

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

with var as (
	select (
			:kode_kabupaten
			-- '32.01'
			-- null
		) kode_kabupaten
		, (
			:kode_kecamatan
			-- '32.01.01'
			-- null
		) kode_kecamatan
		, (
			:kode_kelurahan
 			-- '32.01.01.1001'
			-- null
		) kode_kelurahan
		, (
			:is_desa
			-- true
			-- false
			-- null
		)::bool is_desa
)
, response_merged as (
	select s.*
		, m.*
	from waze.wilayah_merge_kemendagri_2017_bps_2018_kabupaten k
		, waze.wilayah_merge_kemendagri_2017_bps_2018_kecamatan l
		, waze.wilayah_merge_kemendagri_2017_bps_2018_desa m
		, var
		, lateral (
			select (
					k.kemendagri_kabupaten_kode = var.kode_kabupaten
				) kabupaten
				, (
					l.kemendagri_kecamatan_kode = var.kode_kecamatan
				) kecamatan
				, (
					m.kemendagri_desa_kode = var.kode_kelurahan
				) desa
				, (
					m.isdesa = var.is_desa
				) is_desa
		) checking
		, lateral (
			select (
					(
						checking.kabupaten is null
						and true
					)
					or (
						checking.kabupaten is not null
						and checking.kabupaten
					)
				) status_kabupaten
				, (
					(
						checking.kecamatan is null
						and true
					)
					or (
						checking.kecamatan is not null
						and checking.kecamatan
					)
				) status_kecamatan
				, (
					(
						checking.is_desa is null
						and var.is_desa is null
					)
					or (
						checking.is_desa is not null
						and checking.is_desa
					)
				) status_is_desa
				, (
					(
						checking.desa is null
						and true
					)
					or (
						checking.desa is not null
						and checking.desa
					)
				) status
		) s
	where k.kemendagri_kabupaten_kode = l.kemendagri_kabupaten_kode
		and m.kemendagri_kecamatan_kode = l.kemendagri_kecamatan_kode
		and status_kabupaten
		and status_kecamatan
		and status_is_desa
)
, response_data as (
	select kemendagri_desa_kode
		, kemendagri_desa_nama
		, bps_desa_kode
		, bps_desa_nama
		, latitude
		, longitude
		, isdesa is_desa
		$display_geometry
		-- , polygon_json::jsonb->'features'->0->'geometry' geometry
	from response_merged
	where status
	order by kemendagri_desa_nama
)
-- select *
select coalesce(
		jsonb_agg(response_data)
		, '[]'::jsonb
	)
from response_data
;
