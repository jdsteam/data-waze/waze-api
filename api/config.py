class Config(object):
    SSH_HOST = '0.0.0.0'
    SSH_PORT = 0
    SSH_USERNAME = 'username'
    SSH_PASSWORD = 'password'
    SSH_PKEY_FILEPATH = 'ssh_pkey_file_path'
    REMOTE_HOST = 'localhost'
    REMOTE_PORT = 5432
    DB_HOST = 'localhost'
    DB_PORT = 5432
    DB_NAME = 'waze'
    DB_USERNAME = 'postgres'
    DB_PASSWORD = 'password'
    DB_USERNAME_READONLY = 'postgres'
    DB_PASSWORD_READONLY = 'password'
    DB_USERNAME_FULLACCESS = 'postgres'
    DB_PASSWORD_FULLACCESS = 'password'
    DB_SCHEMA = 'waze'