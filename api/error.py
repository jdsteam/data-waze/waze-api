#!/usr/bin/env python3

# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from flask import Blueprint, jsonify, url_for
from werkzeug.exceptions import HTTPException

app = Blueprint('error', __name__)


@app.app_errorhandler(HTTPException)
def default_error(error):
    # Google JSON guide:
    # https://stackoverflow.com/questions/12806386/standard-json-api-response-format
    # HTTP status code for “could not fulfill request for *known* reason”:
    # https://stackoverflow.com/questions/33815690/http-status-code-for-could-not-fulfill-request-for-known-reason
    return jsonify({
        'data': None,
        'error': {
            'code': (code := error.code),
            'message': ' '.join([
                error.description,
                'This is a RESTful Waze-API.',
                'To make it works, please see API doc in here:',
                url_for('doc.show', _external=True)
            ])
        }
    }), code
