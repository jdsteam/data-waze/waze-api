#!/usr/bin/env python3

# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from datetime import date, datetime, timedelta
import json
import os
import re
from string import Template

import geojson
from paramiko import RSAKey
from sshtunnel import SSHTunnelForwarder
from sqlalchemy import (
    bindparam,
    Boolean,
    create_engine,
    Date,
    DateTime,
    Float,
    Integer,
    JSON,
    String
)
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import text
from werkzeug.routing import BaseConverter


string_datetime_format = '%Y-%m-%dT%H:%M:%S'


def converter(object):
    # How to serialize a datetime object as JSON using Python?
    # https://code-maven.com/serialize-datetime-object-as-json-in-python
    if isinstance(object, datetime):
        return object.strftime(string_datetime_format)


def number(
    value,
    default=1,
    is_positive=False,
    number_type=int
):
    try:
        value = number_type(value)

        if is_positive and value <= 0:
            value = default
    except (TypeError, ValueError):
        value = default

    return value


def date_time(
    date_time_value,
    return_milliseconds=False,
    format=string_datetime_format
):
    try:
        if isinstance(date_time_value, str):
            value = datetime.strptime(date_time_value, format)
        else:
            value = date_time_value

        if return_milliseconds:
            value = int(
                value.timestamp() * 1000
            )
    except (AttributeError, TypeError, ValueError):
        value = None

    return value


def map_area(value):
    value = geojson.dumps(value)

    try:
        if geojson.loads(value).is_valid:
            value = json.loads(value)
        else:
            value = None
    except AttributeError:
        value = None

    return value


def list_str(value, return_json=False):
    try:
        value = list(
            set(
                v.strip()
                for v in value.lower().split(',')
                if v.strip()
            )
        )

        if not value:
            value = None
    except AttributeError:
        value = None

    if return_json and value:
        value = json.dumps(value)

    return value


def to_dict(data):
    return dict(
        item
        for item in data.items()
    )


class KodeKemendagriConverter(BaseConverter):
    def to_python(self, value):
        if re.match(r'^\d+$', value):
            value = value, 'provinsi'
        elif re.match(r'^\d+.\d+$', value):
            value = value, 'kabupaten'
        elif re.match(r'^\d+.\d+.\d+$', value):
            value = value, 'kecamatan'
        elif re.match(r'^\d+.\d+.\d+.\d+$', value):
            value = value, 'kelurahan'
        else:
            value = None

        return value

    def to_url(self, value):
        value = self.to_python(value)

        if value:
            value = value[0]
        else:
            value = ''

        return value


class MapJDS():
    list_category = [
        {'name': 'alerts'},
        {'name': 'irregularities'},
        {'name': 'jams'}
    ]

    def __init__(
        self,
        db_name,
        db_username,
        db_password,
        db_schema,
        db_host='localhost',
        db_port=5432,
        ssh_host=None,
        ssh_port=22,
        ssh_username=None,
        ssh_pkey_filepath=None,
        ssh_password=None,
        debug=False
    ):
        for category in self.list_category:
            with open(
                os.path.join(
                    'api', 'sql', f'{ category["name"] }.pgsql'
                ), 'r'
            ) as f:
                category['query'] = f.read()

            with open(
                os.path.join(
                    'api', 'sql', f'aggregate_{ category["name"] }.pgsql'
                ), 'r'
            ) as f:
                category['query_aggregate'] = f.read()

        if ssh_host:
            self.server = SSHTunnelForwarder(
                (ssh_host, ssh_port),
                ssh_username=ssh_username,
                ssh_password=ssh_password,
                # ssh_pkey=RSAKey.from_private_key_file(ssh_pkey_filepath),
                remote_bind_address=(db_host, db_port)
            )
        else:
            self.server = None
            self.db_host = db_host
            self.db_port = db_port

        self.db_name = db_name
        self.db_schema = db_schema
        self.db_username = db_username
        self.db_password = db_password
        self.generate_db()
        self.debug = debug

    def generate_db(self):
        if self.server:
            # SSHTunnelForwarder.daemon_forward_servers is not respected:
            # https://github.com/pahaz/sshtunnel/issues/102
            self.server.daemon_forward_servers = True
            self.server.daemon_transport = True

            self.server.start()
            self.db_host = self.server.local_bind_host
            self.db_port = self.server.local_bind_port

        self.db = create_engine(
            f'''
            postgresql://{self.db_username}:{self.db_password}@{self.db_host}:{self.db_port}/{self.db_name}
            '''.strip(),
            connect_args={'options': f'-csearch_path={self.db_schema}'}
        )
        Session = sessionmaker(bind=self.db)

        try:
            self.session = Session(
                bind=self.db.connect()
            )
        except OperationalError:
            if self.server:
                self.server.stop()

            exit()

    def query(self, string, **kwargs):
        string = text(string)
        args = []

        for key in (
            new_kwargs := dict(
                filter(
                    lambda kwarg: kwarg[0] != 'stream_results',
                    kwargs.items()
                )
            )
        ):
            param = {
                'key': key,
                'type_': String
            }

            for value_type, database_value_type in [
                [int, Integer],
                [float, Float],
                [date, Date],
                [datetime, DateTime],
                [dict, JSON],
                [bool, Boolean],
                [type(None), None]
            ]:
                if isinstance(new_kwargs[key], value_type):
                    param['type_'] = database_value_type
                    break

            args.append(
                bindparam(**param)
            )

        string = string.bindparams(*args)
        string = string.bindparams(**new_kwargs)

        def execute_query(string, kwargs):
            if (
                stream_results := kwargs.get('stream_results')
            ):
                data = self.session.bind.execution_options(
                    stream_results=stream_results
                ).execute(string)
            else:
                data = self.session.execute(string)

            self.session.commit()
            return data

        if self.debug:
            data = execute_query(string, kwargs)
        else:
            try:
                data = execute_query(string, kwargs)
            except Exception:
                self.session.rollback()
                self.session.close()
                self.generate_db()
                data = []

        return data

    def road(self, categories=None, **kwargs):
        found_categories = [
            category
            for category in self.list_category
            if category['name'].lower() in (
                categories
                if categories else []
            )
        ]

        if len(found_categories) == 1:
            if kwargs['map_area']:
                checking = '''
                    ST_Intersects(
                        m."area"
                        , st_geomfromgeojson(var.map_area)
                    )
                '''
            elif kwargs['longitude'] and kwargs['latitude']:
                checking = '''
                    ST_DWithin(
                        m."area"
                        , var.point
                        , var.radius_m
                    )
                '''
            elif kwargs['kode_kabupaten']:
                checking = '''
                    jsonb_build_array(d.kode_kabupaten) <@ var.kode_kabupaten
                '''
            else:
                checking = 'true'

            try:
                value = self.query(
                    Template(
                        found_categories[0]['query']
                    ).substitute(checking=checking),
                    **kwargs
                ).first()[0]
            except AttributeError:
                value = []

            data = {'data': value}
        else:
            start_time = kwargs['start_time']
            end_time = kwargs['end_time']
            data = {
                category['name']: [
                    {
                        key: item[key]
                        for key in item
                    }
                    for item in self.road(
                        [
                            category['name']
                        ],
                        **kwargs
                    )['data']
                ]
                for category in (
                    found_categories
                    if found_categories else self.list_category
                )
            }

            for var, key, method, key_response in [
                [start_time, 'start_time', min, 'startTime'],
                [end_time, 'end_time', max, 'endTime']
            ]:
                if not var:
                    try:
                        var = method(
                            [
                                date_time(
                                    item[key]
                                )
                                for category in data
                                if isinstance(
                                    data[category], list
                                )
                                for item in data[category]
                                if item[key] is not None
                            ]
                        )
                    except ValueError:
                        pass

                data.update({
                    key_response: datetime.strftime(
                        var,
                        string_datetime_format
                    ) if var else var,
                    key_response + 'Millis': date_time(var, True)
                })

        return data

    def generate_aggregate(
        self,
        start_date=None,
        end_date=None,
        kemendagri_kabupaten_kode=None
    ):
        if start_date:
            start_date = date_time(start_date, format='%Y-%m-%d').date()
        else:
            # Dikurangi satu hari, karena aggregate harus lebih lambat satu
            # hari daripada insert roads.
            start_date = date.today() - timedelta(days=1)

        if end_date:
            end_date = date_time(end_date, format='%Y-%m-%d').date()
        else:
            end_date = start_date

        if end_date < start_date:
            _ = start_date
            start_date = end_date
            end_date = _

        if not kemendagri_kabupaten_kode:
            kemendagri_kabupaten_kode = [
                kode[0] for kode in self.query('''
                select w.kemendagri_kabupaten_kode
                from waze.wilayah_merge_kemendagri_2017_bps_2018_kabupaten w
                order by w.kemendagri_kabupaten_nama
                ;
                ''').fetchall()
            ]

        for d in range(
            (end_date - start_date).days + 1
        ):
            aggregate_date = start_date + timedelta(days=d)

            if isinstance(kemendagri_kabupaten_kode, list):
                for kode in kemendagri_kabupaten_kode:
                    self.generate_aggregate(
                        aggregate_date.strftime('%Y-%m-%d'),
                        kemendagri_kabupaten_kode=kode
                    )
            else:
                for category in self.list_category:
                    print(f'''
                Time: { datetime.now() }
                Date Targeted: { aggregate_date }
                Kemendagri's Code for Kabupaten: { kemendagri_kabupaten_kode }
                Generate { category['name'].title() }'s Aggregate: {
                    self.query(
                        category['query_aggregate'],
                        start_time=aggregate_date,
                        kemendagri_kabupaten_kode=kemendagri_kabupaten_kode
                    ).first()[0]
                }
                ------------------------------------------''')
