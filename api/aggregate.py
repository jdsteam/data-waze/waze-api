#!/usr/bin/env python3

# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse

from .library import MapJDS

parser = argparse.ArgumentParser(
    description='''
    A program to generate aggregate jams, alerts, and irregularities of Jawa
    Barat's roads to Jabar Digital Service's Database.
    '''
)
parser.add_argument(
    '--db_name',
    help="set Database's Name",
    type=str,
    metavar='DATABASE',
    default='waze'
)
parser.add_argument(
    '--db_user',
    help="set Database's Username",
    type=str,
    metavar='USER',
    required=True
)
parser.add_argument(
    '--db_pass',
    help="set Database's Password",
    type=str,
    metavar='PASS',
    required=True
)
parser.add_argument(
    '--db_schema',
    help="set Database's Schema",
    type=str,
    metavar='SCHEMA',
    default='waze,public'
)
parser.add_argument(
    '--db_host',
    help="set Database's Host",
    type=str,
    metavar='HOST',
    default='localhost'
)
parser.add_argument(
    '--db_port',
    help="set Database's Port",
    type=int,
    metavar='PORT',
    default=5432
)
parser.add_argument(
    '--ssh_host',
    help="set SSH's Host",
    type=str,
    metavar='SSH_HOST',
    default=None
)
parser.add_argument(
    '--ssh_port',
    help="set SSH's Port",
    type=int,
    metavar='SSH_PORT',
    default=22
)
parser.add_argument(
    '--ssh_user',
    help="set SSH's Username",
    type=str,
    metavar='SSH_USER',
    default=None
)
parser.add_argument(
    '--ssh_pkey',
    help="set SSH's Private Key Filepath",
    type=str,
    metavar='SSH_PKEY',
    default=None
)
parser.add_argument(
    '--debug',
    help="set Debug",
    type=bool,
    default=False
)
parser.add_argument(
    '--start_date',
    help='set Start Date with format YYYY-MM-DD',
    type=str,
    default=None,
    metavar='START'
)
parser.add_argument(
    '--end_date',
    help='set End Date with format YYYY-MM-DD',
    type=str,
    default=None,
    metavar='END'
)
parser.add_argument(
    '--code',
    help="set Kemendagri's Code for Kabupaten",
    type=str,
    default=None,
)
args = parser.parse_args()
map_jds = MapJDS(
    args.db_name,
    args.db_user,
    args.db_pass,
    args.db_schema,
    args.db_host,
    args.db_port,
    args.ssh_host,
    args.ssh_port,
    args.ssh_user,
    args.ssh_pkey,
    args.debug
)
map_jds.generate_aggregate(
    args.start_date,
    args.end_date,
    args.code
)
