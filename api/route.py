#!/usr/bin/env python3

# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
from datetime import datetime, timedelta
import json
from pathlib import Path
from string import Template

from flask import Blueprint, current_app, request, Response, jsonify
from flask_cors import cross_origin

from .library import (
    date_time,
    list_str,
    MapJDS,
    number,
    map_area,
    string_datetime_format,
    to_dict,
    converter
)

app = Blueprint('route', __name__, url_prefix='/')


@app.route('/category/')
@cross_origin(supports_credentials=True)
def category():
    data = [
        item[key]
        for item in MapJDS.list_category
        for key in item
        if key == 'name'
    ]
    data.sort()

    return jsonify(data)


@app.route(area_route := '/area/')
@app.route(kode_area_route := area_route + '<kode_kemendagri:kode_area>/')
@cross_origin(supports_credentials=True)
def area(kode_area=None):
    display_geometry = request.args.get('display_geometry')

    if display_geometry:
        display_geometry = display_geometry.lower().strip()

    if display_geometry != 'true':
        display_geometry = None

    if not kode_area:
        data = kabupaten(display_geometry=display_geometry).json
    elif kode_area[1] == 'kabupaten':
        data = kabupaten(
            kode_area[0],
            display_geometry
        ).json
    elif kode_area[1] == 'kecamatan':
        data = kecamatan(
            '.'.join(
                kode_area[0].split('.')[:2]
            ),
            kode_area[0],
            display_geometry
        ).json
    elif kode_area[1] == 'kelurahan':
        data = kelurahan(
            '.'.join(
                kode_area[0].split('.')[:2]
            ),
            '.'.join(
                kode_area[0].split('.')[:3]
            ),
            kode_area[0],
            display_geometry
        ).json
    else:
        if display_geometry == 'true':
            display_geometry = '''
                , polygon_json::jsonb->'geometry' geometry
            '''
        else:
            display_geometry = ''

        try:
            data = current_app.config['map_jds'].query(
                Template('''
                    with response_data as (
                        select kemendagri_provinsi_kode
                            , kemendagri_provinsi_nama
                            , bps_provinsi_kode
                            , bps_provinsi_nama
                            , latitude
                            , longitude
                            $display_geometry
                        from wilayah_merge_kemendagri_2017_bps_2018_provinsi
                        where kemendagri_provinsi_kode = :kode_area
                        order by kemendagri_provinsi_nama
                    )
                    select coalesce(
                        jsonb_agg(response_data)
                            , '[]'::jsonb
                        )
                    from response_data
                    ;
                ''').substitute(display_geometry=display_geometry),
                kode_area=kode_area[0]
            ).first()[0]
        except AttributeError:
            data = {}

        data = data[0] if data else {}

    return jsonify(data)


@app.route(kode_area_route + 'detail/')
@cross_origin(supports_credentials=True)
def area_detail(
    kode_area,
    display_geometry=None,
    display_level=None,
    is_desa=None
):
    if not display_geometry:
        display_geometry = request.args.get('display_geometry')

    if display_geometry:
        display_geometry = display_geometry.lower().strip()

    if display_geometry != 'true':
        display_geometry = None

    if not display_level:
        display_level = request.args.get('display_level')

    if display_level:
        display_level = display_level.lower().strip()

    if not is_desa:
        is_desa = request.args.get('is_desa')

    if is_desa:
        is_desa = is_desa.lower().strip()

    if is_desa not in ['true', 'false']:
        is_desa = None

    if not kode_area or kode_area[1] == 'kelurahan':
        data = []
    elif kode_area[1] == 'kabupaten':
        data = kecamatan(
            kode_area[0],
            display_geometry=display_geometry
        ).json

        if display_level == 'kelurahan':
            data = [
                c
                for d in data
                for c in kelurahan(
                    '.'.join(
                        d['kemendagri_kecamatan_kode'].split('.')[:2]
                    ),
                    d['kemendagri_kecamatan_kode'],
                    display_geometry=display_geometry,
                    is_desa=is_desa
                ).json
            ]
            data.sort(key=lambda d: d['kemendagri_desa_nama'])
    elif kode_area[1] == 'kecamatan':
        data = kelurahan(
            '.'.join(
                kode_area[0].split('.')[:2]
            ),
            kode_area[0],
            display_geometry=display_geometry,
            is_desa=is_desa
        ).json
    elif kode_area[1] == 'provinsi':
        if display_level == 'kelurahan':
            data = [
                c
                for d in area_detail(
                    kode_area,
                    display_geometry,
                    'kecamatan'
                ).json
                for c in kelurahan(
                    '.'.join(
                        d['kemendagri_kecamatan_kode'].split('.')[:2]
                    ),
                    d['kemendagri_kecamatan_kode'],
                    display_geometry=display_geometry,
                    is_desa=is_desa
                ).json
            ]
            data.sort(key=lambda d: d['kemendagri_desa_nama'])
        else:
            if display_geometry == 'true':
                display_geometry = '''
                    , polygon_json::jsonb->'features'->0->'geometry' geometry
                '''
            else:
                display_geometry = ''

            try:
                data = current_app.config['map_jds'].query(
                    Template('''
                    with response_data as (
                        select kemendagri_kabupaten_kode
                            , kemendagri_kabupaten_nama
                            , bps_kabupaten_kode
                            , bps_kabupaten_nama
                            , latitude
                            , longitude
                            $display_geometry
                        from wilayah_merge_kemendagri_2017_bps_2018_kabupaten
                        where kemendagri_provinsi_kode = :kode_area
                        order by kemendagri_kabupaten_nama
                    )
                    select coalesce(
                        jsonb_agg(response_data)
                            , '[]'::jsonb
                        )
                    from response_data
                    ;
                    ''').substitute(display_geometry=display_geometry),
                    kode_area=kode_area[0]
                ).first()[0]
            except AttributeError:
                data = []

            if display_level == 'kecamatan':
                data = [
                    c
                    for d in data
                    for c in kecamatan(
                        d['kemendagri_kabupaten_kode'],
                        display_geometry='true' if display_geometry else None
                    ).json
                ]
                data.sort(key=lambda d: d['kemendagri_kecamatan_nama'])

    return jsonify(data)


@app.route(kabupate_route := '/kabupaten/')
@app.route(kode_kabupaten_route := kabupate_route + '<kode_kabupaten>/')
@cross_origin(supports_credentials=True)
def kabupaten(
    kode_kabupaten=None,
    display_geometry=None
):
    if kode_kabupaten:
        kode_kabupaten = kode_kabupaten.strip()

    if not display_geometry:
        display_geometry = request.args.get('display_geometry')

    if display_geometry:
        display_geometry = display_geometry.lower().strip()

    if display_geometry == 'true':
        display_geometry = '''
            , polygon_json::jsonb->'features'->0->'geometry' geometry
        '''
    else:
        display_geometry = ''

    with open(
        os.path.join('api', 'sql', 'kabupaten.pgsql'), 'r'
    ) as f:
        data = f.read()

    try:
        data = current_app.config['map_jds'].query(
            Template(data).substitute(display_geometry=display_geometry),
            kode_kabupaten=kode_kabupaten
        ).first()[0]
    except AttributeError:
        data = []

    if kode_kabupaten:
        data = data[0] if data else {}

    return jsonify(data)


@app.route(kode_kabupaten_route + 'aggregate/')
@cross_origin(supports_credentials=True)
def aggregate(kode_kabupaten):
    start_time = date_time(
        request.args.get('start_time')
    )

    if not start_time:
        start_time = datetime.now()

    end_time = date_time(
        request.args.get('end_time')
    )

    if not end_time:
        end_time = start_time

    end_time = end_time + timedelta(days=1)

    if end_time < start_time:
        _ = start_time
        start_time = end_time
        end_time = _

    categories = list_str(
        request.args.get('category')
    )
    list_category = [
        category['name']
        for category in current_app.config['map_jds'].list_category
    ]

    if categories:
        categories = set(
            category for category in categories
            if category in list_category
        )

    if not categories:
        categories = list_category

    data = {}

    for category in categories:
        query_result = current_app.config['map_jds'].query(
            Template('''
                --explain analyze
                select a.*
                from lateral (
                        select *
                        from waze.aggregate_$category
                        where time
                            between :start_time
                            and :end_time
                        order by time
                    ) a
                    , lateral (
                        select *
                        from waze.aggregate_$category
                        where kemendagri_kabupaten_kode = :kode_kabupaten
                        order by kemendagri_kabupaten_kode
                    ) b
                where a.id = b.id
                ;
            ''').substitute(category=category),
            kode_kabupaten=kode_kabupaten,
            start_time=start_time,
            end_time=end_time,
            stream_results=True
        )
        value = []

        # memory-efficient built-in SqlAlchemy iterator/generator:
        # https://stackoverflow.com/questions/7389759/memory-efficient-built-in-sqlalchemy-iterator-generator
        while True:
            batch = query_result.fetchmany(100_000)

            if not batch:
                break

            value.extend(
                dict(
                    item
                    if item[0] not in ('date', 'time')
                    else (
                        (
                            item[0],
                            item[1].strftime('%Y-%m-%d')
                        )
                        if item[0] == 'date'
                        else (
                            item[0],
                            item[1].strftime(string_datetime_format)
                        )
                    )
                    for item in row.items()
                    if item[0] != 'id'
                )
                for row in batch
            )

        query_result.close()
        data[category] = value

    if len(data) == 1:
        data = {'data': list(data.values())[0]}
    else:
        end_time = end_time - timedelta(days=1)
        data.update({
            'startTime': start_time.strftime(string_datetime_format),
            'startTimeMillis': date_time(start_time, True),
            'endTime': end_time.strftime(string_datetime_format),
            'endTimeMillis': date_time(end_time, True)
        })

    return jsonify(data)


@app.route(kecamatan_route := kode_kabupaten_route + 'kecamatan/')
@app.route(kode_kecamatan_route := kecamatan_route + '<kode_kecamatan>/')
@cross_origin(supports_credentials=True)
def kecamatan(
    kode_kabupaten,
    kode_kecamatan=None,
    display_geometry=None
):
    if kode_kabupaten:
        kode_kabupaten = kode_kabupaten.strip()

    if kode_kecamatan:
        kode_kecamatan = kode_kecamatan.strip()

    if not display_geometry:
        display_geometry = request.args.get('display_geometry')

    if display_geometry:
        display_geometry = display_geometry.lower().strip()

    if display_geometry == 'true':
        display_geometry = '''
            , polygon_json::jsonb->'features'->0->'geometry' geometry
        '''
    else:
        display_geometry = ''

    with open(
        os.path.join('api', 'sql', 'kecamatan.pgsql'), 'r'
    ) as f:
        data = f.read()

    try:
        data = current_app.config['map_jds'].query(
            Template(data).substitute(display_geometry=display_geometry),
            kode_kabupaten=kode_kabupaten,
            kode_kecamatan=kode_kecamatan
        ).first()[0]
    except AttributeError:
        data = []

    if kode_kecamatan:
        data = data[0] if data else {}

    return jsonify(data)


@app.route(kelurahan_route := kode_kecamatan_route + 'kelurahan/')
@app.route(kelurahan_route + '<kode_kelurahan>/')
@cross_origin(supports_credentials=True)
def kelurahan(
    kode_kabupaten,
    kode_kecamatan,
    kode_kelurahan=None,
    display_geometry=None,
    is_desa=None
):
    if kode_kabupaten:
        kode_kabupaten = kode_kabupaten.strip()

    if kode_kecamatan:
        kode_kecamatan = kode_kecamatan.strip()

    if kode_kelurahan:
        kode_kelurahan = kode_kelurahan.strip()

    if not display_geometry:
        display_geometry = request.args.get('display_geometry')

    if display_geometry:
        display_geometry = display_geometry.lower().strip()

    if display_geometry == 'true':
        display_geometry = '''
            , polygon_json::jsonb->'features'->0->'geometry' geometry
        '''
    else:
        display_geometry = ''

    if not is_desa:
        is_desa = request.args.get('is_desa')

    if is_desa:
        is_desa = is_desa.lower().strip()

    if is_desa == 'true':
        is_desa = True
    elif is_desa == 'false':
        is_desa = False
    else:
        is_desa = None

    with open(
        os.path.join('api', 'sql', 'desa.pgsql'), 'r'
    ) as f:
        data = f.read()

    try:
        data = current_app.config['map_jds'].query(
            Template(data).substitute(display_geometry=display_geometry),
            kode_kabupaten=kode_kabupaten,
            kode_kecamatan=kode_kecamatan,
            kode_kelurahan=kode_kelurahan,
            is_desa=is_desa
        ).first()[0]
    except AttributeError:
        data = []

    if kode_kelurahan:
        data = data[0] if data else {}

    return jsonify(data)


@app.route('/', methods=['GET', 'POST'])
@cross_origin(supports_credentials=True)
def main():
    start_time = date_time(
        request.args.get('start_time')
    )
    end_time = date_time(
        request.args.get('end_time')
    )

    if start_time and end_time and end_time < start_time:
        _ = start_time
        start_time = end_time
        end_time = _

    return jsonify(
        current_app.config['map_jds'].road(
            list_str(
                request.args.get('category')
            ),
            **{
                'map_area': map_area(
                    request.get_json(silent=True)
                ),
                'start_time': start_time,
                'end_time': end_time,
                'kode_kabupaten': list_str(
                    request.args.get('kode_kabupaten'),
                    True
                ),
                'page': number(
                    request.args.get('page'),
                    is_positive=True
                ),
                'array_size': number(
                    request.args.get('array_size'),
                    800,
                    True
                ),
                'longitude': number(
                    request.args.get('longitude'),
                    None,
                    number_type=float
                ),
                'latitude': number(
                    request.args.get('latitude'),
                    None,
                    number_type=float
                ),
                'radius_km': number(
                    request.args.get('radius_km'),
                    1.,
                    True,
                    float
                )
            }
        )
    )


@app.route('/license/')
@cross_origin(supports_credentials=True)
def license():
    return (
        open('LICENSE', 'r').read(),
        200,
        {
            'Content-Type': 'text/plain'
        }
    )


@app.route('/banjir/')
@cross_origin(supports_credentials=True)
def banjir():
    if not (
        start_time := date_time(
            request.args.get('start_time')
        )
    ):
        start_time = datetime.now() - timedelta(days=1)

    if not (
        end_time := date_time(
            request.args.get('end_time')
        )
    ):
        end_time = datetime.now()

    if start_time and end_time and end_time < start_time:
        _ = start_time
        start_time = end_time
        end_time = _

    query_result = current_app.config['map_jds'].query(
        Path(
            os.path.join(
                current_app.root_path,
                'sql',
                'banjir_jabar.pgsql'
            )
        ).read_text(),
        start_time=start_time,
        end_time=end_time,
        stream_results=True
    )

    def generate(query_result):
        # Python: Using Flask to stream chunked dynamic content to end users
        # https://fabianlee.org/2019/11/18/python-using-flask-to-stream-chunked-dynamic-content-to-end-users/
        # Streaming Contents
        # https://flask.palletsprojects.com/en/1.1.x/patterns/streaming/#basic-usage
        # Streaming JSON with Flask
        # https://blog.al4.co.nz/2016/01/streaming-json-with-flask/
        # How to serialize a datetime object as JSON using Python?
        # https://code-maven.com/serialize-datetime-object-as-json-in-python
        yield '{"data": ['

        while True:
            batch = query_result.fetchmany(100_000)

            if not batch:
                break

            releases = batch.__iter__()
            prev_release = next(releases)

            for release in releases:
                yield json.dumps(
                    to_dict(
                        prev_release
                    ),
                    default=converter
                ) + ', '
                prev_release = release

            yield json.dumps(
                to_dict(prev_release),
                default=converter
            )

        yield ']}'

    return Response(
        generate(query_result),
        mimetype='application/json'
    )
