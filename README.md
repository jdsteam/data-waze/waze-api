# API Waze

<!-- TOC -->

- [API Waze](#api-waze)
    - [Requirements](#requirements)
    - [Usefull Command](#usefull-command)
    - [Source-code Editor](#source-code-editor)
        - [Extension](#extension)
        - [Pengaturan](#pengaturan)
            - [`{{root}}/.markdownlint.json`](#rootmarkdownlintjson)
            - [`{{root}}/.vscode/settings.json`](#rootvscodesettingsjson)
            - [`{{root}}/.vscode/launch.json`](#rootvscodelaunchjson)
    - [Referensi](#referensi)
    - [Lisensi](#lisensi)

<!-- /TOC -->

> [Dokumentasi penggunaan API Waze](https://documenter.getpostman.com/view/5370600/SVtSWUz5)

---

## _Requirements_

- [Python 3](https://www.python.org/downloads/) dipergunakan untuk menjalankan
_API_.
- Pastikan [_library_](api/requirements.txt) Python telah ter-_install_.
- Pastikan [PostGIS](https://postgis.net/) telah ter-_install_ di _database
server_, karena akan dipergunakan untuk _query_.
- Buat _file_ `{{root}}/api/config.py` dan isi dengan data _server_.

    ```python
    class Config(object):
        DB_NAME = 'waze'
        DB_USERNAME_READONLY = 'usernamse'
        DB_PASSWORD_READONLY = 'password'
        DB_USERNAME_FULLACCESS = 'usernamse'
        DB_PASSWORD_FULLACCESS = 'password'
        DB_SCHEMA = 'waze,public'
        REMOTE_HOST = '0.0.0.0'
        REMOTE_PORT = 5432
        SSH_HOST = '0.0.0.0'
        SSH_PORT = 22
        SSH_USERNAME = 'username'
        SSH_PASSWORD = 'password'
        SSH_PKEY_FILEPATH = 'ssh_pkey_file_path'
    ```

- Salin isi dari _clipboard command_ berikut ini ke bagian
[`Settings > CI/CD > Variables` repositori](https://gitlab.com/jdsteam/data-waze/waze-api/-/settings/ci_cd).

    ```shell
    $ cat api/config.py | base64 | pbcopy
    ```

    > Beri nama _varibale_ `CONFIGPY`.

- Buat _file_ `{{root}}/api/id_rsa` dan isi dengan _SSH private key_.
- GitLab CI/CD dipergunakan dalam proses _deployment_ ke _server_ dengan
_Specific Runner_ dan _executor docker_.
    > Perlu dilakukan penyesuaian pada konfigurasi _runner_ `/etc/gitlab-runner/config.toml`.

    ```toml
    ...
    [[runners]]
        ...
        [runners.docker]
            ...
            privileged = true
            ...
    ```

- Ada penyesuaian pada file `/etc/nginx/nginx.conf`

    ```conf
    ...
    events {
        ...
        multi_accept on;
    }

    http {
        ...
        keepalive_timeout 400;
        ...
        server_tokens off;

        server_names_hash_bucket_size 64;
        ...
        gzip_vary on;
        gzip_proxied any;
        gzip_comp_level 5;
        ...
        gzip_http_version 1.1;
        gzip_min_length 256;
        gzip_types
            application/atom+xml
            application/javascript
            application/json
            application/rss+xml
            application/vnd.ms-fontobject
            application/x-font-ttf
            application/x-web-app-manifest+json
            application/xhtml+xml
            application/xml
            font/opentype
            image/svg+xml
            image/x-icon
            text/css
            text/plain
            text/x-component
            text/javascript
            text/xml;
        ...
    ```

- Ada penyesuaian pada file `/etc/nginx/sites-available/default`

    ```conf
    ...
    server {
        listen 80;
        server_name waze-api.18.140.161.219.xip.io;

        location / {
            proxy_set_header Host $host;
            proxy_pass http://0.0.0.0:14611;
            proxy_redirect off;
            proxy_connect_timeout 400;
            proxy_read_timeout 400;
        }
    }
    ...
    ```

---

## _Usefull Command_

- _Run bash's script for generate aggregation of roads (alerts, jams, and
irregularities) to JDS's Database_:

    ```shell
    $ python -m api.aggregate \
    --db_user {{USER}} \
    --db_pass {{PASS}} \
    --db_host {{HOST}} \
    --ssh_host {{SSH_HOST}} \
    --ssh_user {{SSH_USER}} \
    --ssh_pkey {{SSH_PKEY}} \
    --start_date {{DATE}}
    ```

- [Menyelesaikan masalah _ssh “permissions are too open” error_](https://stackoverflow.com/questions/9270734/ssh-permissions-are-too-open-error):

    ```shell
    $ chmod 400 {{ssh_pkey_file_path}}
    ```

- [Masuk ke _production server_](https://serverpilot.io/docs/how-to-use-ssh-public-key-authentication):

    ```shell
    $ ssh -i {{ssh_pkey_file_path}} {{ssh_username}}@{{ssh_host}}
    ```

- Membuat _image docker_:

    ```shell
    $ docker build --tag waze-api .
    ```

- Melihat daftar _container docker_ yang sedang dipergunakan:

    ```shell
    $ docker ps
    ```

- Menjalankan _container docker_:
    - Flask _app_:

        ```shell
        $ docker run --detach -p 80:5000 waze-api
        ```

    - _Image docker_ secara umum:

        ```shell
        $ docker run -it {{image_id}}
        ```

    - [_Starting a shell in the Docker Alpine container_](https://stackoverflow.com/questions/35689628/starting-a-shell-in-the-docker-alpine-container):

        ```shell
        $ docker run -it --rm {{image_id}} /bin/ash
        ```

- [Masuk ke dalam _container docker_ yang sedang berjalan](https://stackoverflow.com/questions/20932357/how-to-enter-in-a-docker-container-already-running-with-a-new-tty):

    ```shell
    $ docker exec -it {{container_id}} bash
    ```

- Mengakhiri jalannya _container docker_:

    ```shell
    $ docker stop {{container_id}}
    ```

- Melihat daftar _image docker_ yang sedang dipergunakan:

    ```shell
    $ docker images
    ```

- Menghapus _image docker_:

    ```shell
    $ docker rmi -f {{image_id}}
    ```

---

## _Source-code Editor_

[VSCode](https://code.visualstudio.com/) (Visual Studio Code) dipergunakan untuk
memudahkan dalam penulisan _source-code_.

### _Extension_

Beberapa _extension_ juga perlu dipasangkan ke VSCode:

- [_Auto Markdown TOC_](https://marketplace.visualstudio.com/items?itemName=huntertran.auto-markdown-toc)
- [_Markdownlint_](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
- ~~[_Markdown Preview with Bitbucket Styles_](https://marketplace.visualstudio.com/items?itemName=hbrok.markdown-preview-bitbucket)~~
- [_Python extension for Visual Studio Code_](https://marketplace.visualstudio.com/items?itemName=ms-python.python)

### Pengaturan

#### `{{root}}/.markdownlint.json`

```json
{
    "MD007": false,
    "MD013": {
        "code_blocks": false
    },
    "MD014": false
}
```

Aturan penggunaannya dapat dilihat [di sini](https://github.com/markdownlint/markdownlint/blob/master/docs/RULES.md).

#### `{{root}}/.vscode/settings.json`

```json
{
    "python.linting.flake8Enabled": true,
    "python.linting.flake8Args": ["--ignore=E203,E231,W503"],
    "python.pythonPath": "${workspaceFolder}/.env/bin/python",
    "files.exclude": {
        "**/.classpath": true,
        "**/.project": true,
        "**/.settings": true,
        "**/.factorypath": true
    },
}
```

#### `{{root}}/.vscode/launch.json`

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Flask",
            "type": "python",
            "request": "launch",
            "module": "flask",
            "env": {
                "FLASK_APP": "app/api",
                "FLASK_ENV": "development",
                "FLASK_DEBUG": "0"
            },
            "args": [
                "run",
                "--no-debugger",
                "--no-reload",
                "--port",
                "80",
                "--host",
                "0.0.0.0"
            ],
            "jinja": true
        }
    ]
}
```

---

## Referensi

- [_Deploying a scalable Flask app using Gunicorn and Nginx, in Docker: Part #1_](https://medium.com/@kmmanoj/deploying-a-scalable-flask-app-using-gunicorn-and-nginx-in-docker-part-1-3344f13c9649)
- [_Export secret file to Gitlab pipeline_](https://medium.com/@michalkalita/export-secret-file-to-gitlab-pipeline-75789eee35bd)
- [_Deploying WSGI Python applications using Docker_](https://cobuildlab.com/development-blog/deploying-python-wsgi-application-using-Docker/)
- [_A scalable Flask application using Gunicorn on Ubuntu 18.04 in Docker_](https://philchen.com/2019/07/09/a-scalable-flask-application-using-gunicorn-on-ubuntu-18-04-in-docker)
- [_Docker remove none TAG images_](https://stackoverflow.com/questions/33913020/docker-remove-none-tag-images)
- [_Deploying Flask Apps Easily with Docker and Nginx_](https://ianlondon.github.io/blog/deploy-flask-docker-nginx/)
- [_The Flask Mega-Tutorial Part XIX: Deployment on Docker Containers_](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xix-deployment-on-docker-containers)
- [_Building Minimal Docker Containers for Python Applications_](https://blog.realkinetic.com/building-minimal-docker-containers-for-python-applications-37d0272c52f3)
- [_Benchmarking Debian vs Alpine as a Base Docker Image_](https://nickjanetakis.com/blog/benchmarking-debian-vs-alpine-as-a-base-docker-image)
- [_Deploying a python-django application using docker_](https://dev.to/lewiskori/deploying-a-python-django-application-using-docker-3d09)
- [_How to use Django, PostgreSQL, and Docker_](https://wsvincent.com/django-docker-postgresql/)
- [_Here's a Production-Ready Dockerfile for Your Python/Django App_](https://www.caktusgroup.com/blog/2017/03/14/production-ready-dockerfile-your-python-django-app/)
- [_Hello World - Python_](https://knative.dev/docs/serving/samples/hello-world/helloworld-python/)
- [_Slimming Down Your Docker Images_](https://towardsdatascience.com/slimming-down-your-docker-images-275f0ca9337e)
- [_Alpine Based Docker Images Make a Difference in Real World Apps_](https://blog.codeship.com/alpine-based-docker-images-make-difference-real-world-apps/)
- [_Setting Up GitLab CI for a Python Application_](http://www.patricksoftwareblog.com/setting-up-gitlab-ci-for-a-python-application/)
- [_Building Django Docker Image with Alpine_](https://medium.com/c0d1um/building-django-docker-image-with-alpine-32de65d2706)
- [_Cannot “pip install cryptography” in Docker Alpine Linux 3.3 with OpenSSL
1.0.2g and Python 2.7_](https://stackoverflow.com/questions/35736598/cannot-pip-install-cryptography-in-docker-alpine-linux-3-3-with-openssl-1-0-2g)
- [_Installing psycopg2-binary with Python:3.6.4-alpine doesn't work_](https://github.com/psycopg/psycopg2/issues/684)
- [_Reduce Docker image sizes using Alpine_](https://www.sandtable.com/reduce-docker-image-sizes-using-alpine/)
- [_How to Write Dockerfiles for Python Web Apps_](https://blog.hasura.io/how-to-write-dockerfiles-for-python-web-apps-6d173842ae1d/)
- [_Building small python Docker images, How to?_](https://medium.com/bytes-mania/building-small-python-docker-images-how-to-8bb539d3e6fc)

---

## Lisensi

Lisensi yang dipergunakan adalah [MIT](LICENSE).
